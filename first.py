from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import pprint

for cod in range(5000, 6000):
    site = "http://www.alunoonline.uerj.br/requisicaoaluno/requisicao.php?requisicao=DadosDisciplina&disciplinas[0]="+str(cod)
    hdr = {'User-Agent': 'Mozilla/5.0'}
    req = Request(site, headers=hdr)
    page = urlopen(req)
    res = BeautifulSoup(page, 'html5lib')

    tags = res.findAll('div')
    # pprint.pprint(tags[3])

    if len(tags) > 3:
        print(tags[3].getText())
